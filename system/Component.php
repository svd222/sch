<?php
namespace System;
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 16.08.18
 * Time: 23:38
 */
class Component
{
    public function __get($name)
    {
        $methodName = 'get' . ucfirst($name);
        if (method_exists($this, $methodName)) {
            return $this->$methodName();
        }
        return null;
    }

    public function __set($name, $value)
    {
        $methodName = 'set' . ucfirst($name);
        if (method_exists($this, $methodName)) {
            $this->$methodName($value);
        }
    }
}