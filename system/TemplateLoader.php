<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 16.08.18
 * Time: 22:37
 */
namespace System;

use Symfony\Component\Templating\Loader\LoaderInterface;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Helper\SlotsHelper;
use Symfony\Component\Templating\EngineInterface;

class TemplateLoader {
    /**
     *
     * @return EngineInterface
     */
    public function load(LoaderInterface $loader)
    {
        $templating = null;
        switch (Sch::$app->configuration['template']['engine']) {
            case "PhpEngine":
            default: {
                $templating = new PhpEngine(new TemplateNameParser(), $loader);
                break;
            }
        }
        if ($templating) {
            $templating->set(new SlotsHelper());
        } else {
            throw new \InvalidArgumentException('unknown template engine');
        }

        return $templating;
    }
}
