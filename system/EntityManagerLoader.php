<?php
namespace System;
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 21.08.18
 * Time: 20:38
 */
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;;

class EntityManagerLoader
{
    private $entityManager;

    public function __construct(array $configuration)
    {
        $paths = array(__DIR__."/../src/Entity");

        $isDevMode = true;

        $dbParams = $configuration['db'];
        /*array(
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '',
            'dbname'   => 'foo',
        );*/

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        $this->entityManager = EntityManager::create($dbParams, $config);
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }
}

