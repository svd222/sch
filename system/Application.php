<?php
namespace System;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\Loader\FilesystemLoader;

/**
 * Created by PhpStorm.
 * User: svd
 * Date: 17.08.18
 * Time: 0:05
 */
class Application extends Component
{
    private static $app;

    private $configuration;

    /**
     * @var EntityManager
     */
    private $entityManager;

    private function __construct()
    {

    }

    public function setConfiguration($value) {
        $this->configuration = $value;
    }

    public function getConfiguration($key = '', $default = null) {
        $val = null;
        if (!$key) {
            $val = $this->configuration;
        } else {
            if (isset($this->configuration[$key])) {
                $val = $this->configuration[$key];
            }
        }
        return $val;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getInstance()
    {
        if (!self::$app) {
            self::$app = new self();
        }
        return self::$app;
    }

    public function runAction($params)
    {
        $controller = $params['_controller'];
//        $route = $params['_route'];
        $controllerParams = explode('::', $controller);
        $controller = $controllerParams[0];
        $action = 'indexAction';
        if (count($controllerParams) == 2) {
            $action = $controllerParams[1];
        }
        if (strpos($controller, '\\') === false) {
            $controller = 'App\\Controller\\' . $controller;
        }

        $request = Request::createFromGlobals();

        $filesystemLoader = new FilesystemLoader(__DIR__.'/../src/views/%name%');
        $templateLoader = new TemplateLoader();
        $templating = $templateLoader->load($filesystemLoader);

        $func = array(new $controller($request, $templating), $action);
        return $func();
    }
}