<?php
namespace System;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Created by PhpStorm.
 * User: svd
 * Date: 19.08.18
 * Time: 23:02
 */
class Controller extends Component
{
    /**
     * @var Request $request
     */
    protected $request;

    protected $config;

    /**
     * @var EngineInterface
     */
    protected $templating;

    public function __construct(Request $request, EngineInterface $templating)
    {
        $this->request = $request;

        $this->templating = $templating;
    }

    public function render(string $view, $params = [])
    {
        return $this->templating->render($view, $params);
    }
}