<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 16.08.18
 * Time: 22:38
 */
return [
    'db' => [
        'driver'   => 'pdo_mysql',
        'user'     => 'root',
        'password' => 'svd',
        'dbname'   => 'schedule',
    ],
    'template' => [
        'engine' => 'PhpEngine',
    ]
];