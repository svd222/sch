<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 16.08.18
 * Time: 21:03
 */
use System\Sch;
use System\Application;
use System\EntityManagerLoader;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\NoConfigurationException;

require_once "vendor/autoload.php";

$confPath = realpath('./config/config.php');
Sch::$app = Application::getInstance();
Sch::$app->configuration = require_once $confPath;

$fileLocator = new FileLocator(array(__DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'routes'));
$loader = new YamlFileLoader($fileLocator);
$routes = $loader->load('routes.yaml');

$context = new RequestContext('/');
$matcher = new UrlMatcher($routes, $context);

Sch::$app->entityManager = (new EntityManagerLoader(Sch::$app->configuration))->getEntityManager();

$parameters = null;
try {
    $parameters = $matcher->match($_SERVER['REQUEST_URI']); //('/foo');
} catch (NoConfigurationException $e) {
    die('The page you are looking for does not exist.');
}

/**
 * @var Response $response
 */
$response = Sch::$app->runAction($parameters);
echo $response;


