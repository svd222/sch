<?php
namespace App\Util;
use App\Entity\Courier;
use App\Entity\City;

/**
 * Created by PhpStorm.
 * User: svd
 * Date: 20.08.18
 * Time: 21:00
 */
class ScheduleGenerator
{
    private $startDate;

    private $endDate;

    private $couriers;

    private $cities;

    private $couriersEnRoute;

    private $limit;

    private $schedule;

    /**
     * ScheduleGenerator constructor.
     * @param string $startDate in YYYY-mm-dd format
     * @param string $endDate in YYYY-mm-dd format
     * @param array $couriers
     * @param City[] $cities
     */
    public function __construct($startDate, array $couriers, array $cities)
    {
        $this->startDate = \DateTime::createFromFormat('Y-m-d', $startDate);
        $this->endDate = new \DateTime();
        $this->limit = $this->endDate->diff($this->startDate)->days * 24;
        $this->couriers = $couriers;
        $this->cities = $cities;

        $this->couriersEnRoute = new \App\Util\CourierMinHeap();
        for ($i = 0; $i < count($couriers); $i++) {
            $this->couriersEnRoute->insert($this->couriers[$i]);
        }

        $this->schedule = [];
    }

    protected function sendNextCourier()
    {
        /**
         * @var Courier $nextPotentialCourier
         */
        $nextPotentialCourier = $this->couriersEnRoute->extract();

        $result = $nextPotentialCourier->send($this->cities[mt_rand(0, count($this->cities) - 1)], $this->limit);
        if ($result) {
            $this->couriersEnRoute->insert($nextPotentialCourier);
        } else {
            array_push($this->schedule, $nextPotentialCourier);
        }
        if (count($this->couriersEnRoute) == 0) {
            return false;
        }

        return true;
    }

    public function generate(): array
    {
        do {
            $result = $this->sendNextCourier();
        } while ($result);
        return $this->schedule;
    }
}