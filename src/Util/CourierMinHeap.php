<?php
namespace App\Util;
use App\Entity\Courier;

/**
 * Created by PhpStorm.
 * User: svd
 * Date: 20.08.18
 * Time: 22:46
 */
class CourierMinHeap extends \SplMinHeap
{
    /**
     * @param Courier $value1
     * @param Courier $value2
     * @return int
     */
    protected function compare($value1, $value2)
    {
        if ($value1->traveledDistance < $value2->traveledDistance) {
            return 1;
        } else if ($value1->traveledDistance > $value2->traveledDistance) {
            return -1;
        } else {
            return 0;
        }
    }
}