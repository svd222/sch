<?php
namespace App\Controller;

use App\Entity\City;
use App\Entity\Courier;
use App\Entity\Schedule;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\GreaterThan;
use System\Controller;
use System\Sch;
use Faker\Factory;
use App\Util\ScheduleGenerator;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\NotBlank;

class ScheduleController extends Controller
{
    /**
     *
     * @return string
     */
    public function generateAction()
    {
        $em = Sch::$app->entityManager;
        $couriersList = $em->getRepository('App\Entity\Courier')->findAll();
        $citiesList = $em->getRepository('App\Entity\City')->findAll();
        $startDate = new \DateTime('2015-06-01 00:00:00');

        $generator = new ScheduleGenerator('2015-08-01', $couriersList, $citiesList);
        $schedule = $generator->generate();
        $counter = 0;
        foreach ($schedule as $courier) {
            /**
             * @var Courier $courier
             */
            $cDate = $startDate;
            foreach ($courier->path as $city) {
                /**
                 * @var City $city
                 */
                $sch = new Schedule();
                $sch->setDepartureDate($cDate);
                $nDate = $cDate->add(new \DateInterval("PT".$city->getDistanceToMoscowInHours()."H"));
                $sch->setArrivalDate($nDate);
                $sch->setCity($city);
                $sch->setCourier($courier);
                $em->persist($sch);
                $counter++;
                $em->flush();
                $cDate = $nDate;
                if ($counter % 100 == 0) {
                    break 2;
                }
            }
        }
        $em->flush();

        return $this->render('test.php', []);
    }

    public function addAction()
    {
        $post = $this->request->request;

        $message = '';
        $errors = [];
        if ($post->get('submit')) {
            //form is submitted
            $departureDate = $post->get('departureDate');
            $arrivalDate = $post->get('arrivalDate');
            $city_id = $post->get('city');
            $courier_id = $post->get('courier');


            /** @var EntityManager $em */
            $em = Sch::$app->entityManager;

            $validator = Validation::createValidator();

            $violations = $validator->validate($departureDate, array(
                new NotBlank(),
                new DateTime()
            ));

            if (empty($violations)) {
                $violations = $validator->validate($arrivalDate, array(
                    new NotBlank(),
                    new DateTime()
                ));
            }

            if (empty($violations)) {
                $violations = $validator->validate($city_id, array(
                    new GreaterThan(['value' => 0])
                ));
            }

            if (empty($violations)) {
                $violations = $validator->validate($courier_id, array(
                    new GreaterThan(['value' => 0])
                ));
            }


            if (0 !== count($violations)) {
                foreach ($violations as $violation) {
                    $errors = $violation->getMessage().'<br>';

                }
            } else {
                $schedule = new Schedule();
                $schedule->setCityId($city_id);
                $schedule->setCourierId($courier_id);
                $schedule->setArrivalDate($arrivalDate);
                $schedule->setDepartureDate($departureDate);
                $em->persist($schedule);
                $em->flush();
                $message = 'Entity successful stored!';
            }
        }

        $em = Sch::$app->entityManager;
        $cities = $em->getRepository('App\Entity\City')->findAll();
        return $this->render('add.php', [
            'cities' => $cities,
            'errors' => $errors,
            'message' => $message
        ]);
    }

    public function getAvaialableCouriersAction()
    {
        function leadZero(&$array) {
            array_walk($array, function (&$dtItem, $key) {
                if ($dtItem < 10) {
                    $dtItem = '0'.$dtItem;
                }
            });
        }

        $dt = $this->request->request->get('dt');

        $dt = explode(' ', $dt);
        $date = explode('-', $dt[0]);
        array_walk($date, function (&$dtItem, $key) { //correct month
            if ($key == 1) {
                $dtItem += 1;
            }
        });
        leadZero($date);
        $time = $dt[1];
        $time = explode(':', $time);
        leadZero($time);
        $date = implode('-',$date);
        $time = implode(':', $time);
        $dt = implode(' ', [$date, $time]);//YYYY-mm-dd HH:ii

        $sql = "SELECT * FROM `schedule` WHERE '".$dt."' > departure_date AND '".$dt."' < arrival_date";
        $em = Sch::$app->entityManager;

        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata('App\Entity\Schedule', 'schedule');

        $query = $em->createNativeQuery($sql, $rsm);
        $schedule = $query->getResult();

        $couriersList = array_map(function ($courier) {
            /**
             * @var Courier $courier
             */
            return [$courier->id, $courier->surname];
        }, $em->getRepository('App\Entity\Courier')->findAll());

        foreach ($schedule as $sch) {
            /**
             * @var Schedule $sch
             */
            $key = null;
            foreach ($couriersList as $k => $c) {
                if ($c[0] == $sch->courier->id) {
                    $key = $k;
                    break;
                }
            }

            if ($key !== null) {
                unset($couriersList[$key]);
            }
        }
        sort($couriersList);
        return json_encode($couriersList);
    }

    public function getCityAction()
    {
        $cityId = $this->request->request->get('city_id');

        $em = Sch::$app->entityManager;
        $city = $em->getRepository('App\Entity\City')->find($cityId);
        $dst = 0;
        if ($city) {
            $dst = $city->getDistanceToMoscowInHours();
        }
        return json_encode($dst);
    }

    public function listAction()
    {
        return $this->render('test.php', []);
    }

    /**
     * Filling the cities table. Alternative way - use of Doctrine DBAL, but count of cities is small, therefore we can use Doctrine ORM for this purpose.
     */
    public function fillCitiesAction()
    {
        /** @var EntityManager $em */
        $em = Sch::$app->entityManager;

        $cityRepository = $em->getRepository('App\Entity\City');
        $cities = $cityRepository->findAll();

        $cityNames = ['Санкт-Петербург', 'Уфа', 'Нижний Новгород', 'Владимир', 'Кострома', 'Екатеринбург', 'Ковров', 'Воронеж', 'Самара', 'Астрахань'];

        if (empty($cities)) {
            foreach ($cityNames as $name) {
                $city = new City;
                $city->name = $name;
                $city->distanceToMoscowInHours = mt_rand(4, 20);
                $em->persist($city);
                $cities[] = $city;
            }
            $em->flush();
        }

        return $this->render('city_list.php', ['cities' => $cities]);
    }

    /**
     * Filling the couriers table. Alternative way - use of Doctrine DBAL, but count of couriers is small, therefore we can use Doctrine ORM for this purpose.
     */
    public function fillCouriersAction()
    {
        /** @var EntityManager $em */
        $em = Sch::$app->entityManager;

        $courierRepository = $em->getRepository('App\Entity\Courier');
        $couriers = $courierRepository->findAll();
        $couriersCount = mt_rand(10, 20);

        if (empty($couriers)) {
            $faker = Factory::create();
            for ($i = 0; $i < $couriersCount; $i++) {
                $courier = new Courier;
                $courier->surname = $faker->name;
                $courier->traveledDistance = 0;
                $em->persist($courier);
                $couriers[] = $courier;
            }
            $em->flush();
        }

        return $this->render('courier_list.php', ['couriers' => $couriers]);
    }
}