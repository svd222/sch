<?php

/**
 * Created by PhpStorm.
 * User: svd
 * Date: 20.08.18
 * Time: 23:29
 */

namespace App\Entity;

use System\Component;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="couriers")
 **/
class Courier extends Component
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $surname;

    protected $traveledDistance;

    private $path = [];

    public function getPath()
    {
        return $this->path;
    }

    /**
     * @OneToMany(targetEntity="App\Entity\Schedule", mappedBy="courier", cascade={"remove"}, fetch="EAGER")
     */
    protected $schedules;

    public function __construct()
    {
        $this->schedules = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Courier
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set traveledDistance
     *
     * @param integer $traveledDistance
     *
     * @return Courier
     */
    public function setTraveledDistance($traveledDistance)
    {
        $this->traveledDistance = $traveledDistance;
    }

    /**
     * Get traveledDistance
     *
     * @return integer
     */
    public function getTraveledDistance()
    {
        return $this->traveledDistance;
    }

    /**
     * Checks whether the courier can go to the city
     *
     * @param City $city
     * @param $limit
     * @return bool
     */
    protected function canBeSend(City $city, $limit): bool
    {
        return $this->traveledDistance + $city->distanceToMoscowInHours < $limit;
    }

    /**
     * Sends the courier to city
     *
     * @param City $city
     * @param $limit
     * @return bool
     */
    public function send(City $city, $limit): bool
    {
        if ($this->canBeSend($city, $limit)) {
            $this->path = array_merge($this->path, [$city]);
            $this->traveledDistance += $city->distanceToMoscowInHours;
            return true;
        }
        return false;
    }
}