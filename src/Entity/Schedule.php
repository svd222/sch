<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 22.08.18
 * Time: 3:02
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use System\Component;
use App\Entity\Courier;
use App\Entity\City;

/**
 * @Entity @Table(name="schedule")
 **/
class Schedule extends Component
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /**
     * @Column(type="datetime")
     */
    protected $departure_date;

    /**
     * @Column(type="datetime")
     */
    protected $arrival_date;

    /**
     * @ManyToOne(targetEntity="App\Entity\City", cascade={"all"}, fetch="EAGER")
     */
    protected $city;

    /**
     * @ManyToOne(targetEntity="App\Entity\Courier", cascade={"all"}, fetch="EAGER")
     */
    protected $courier;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set departureDate
     *
     * @param \DateTime $departureDate
     *
     * @return Schedule
     */
    public function setDepartureDate($departureDate)
    {
        $this->departure_date = $departureDate;

        return $this;
    }

    /**
     * Get departureDate
     *
     * @return \DateTime
     */
    public function getDepartureDate()
    {
        return $this->departure_date;
    }

    /**
     * Set arrivalDate
     *
     * @param \DateTime $arrivalDate
     *
     * @return Schedule
     */
    public function setArrivalDate($arrivalDate)
    {
        $this->arrival_date = $arrivalDate;

        return $this;
    }

    /**
     * Get arrivalDate
     *
     * @return \DateTime
     */
    public function getArrivalDate()
    {
        return $this->arrival_date;
    }

    /**
     * Set cityId
     *
     * @param integer $cityId
     *
     * @return Schedule
     */
    public function setCityId($cityId)
    {
        $this->city_id = $cityId;

        return $this;
    }

    /**
     * Get cityId
     *
     * @return integer
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * Set courierId
     *
     * @param integer $courierId
     *
     * @return Schedule
     */
    public function setCourierId($courierId)
    {
        $this->courier_id = $courierId;

        return $this;
    }

    /**
     * Get courierId
     *
     * @return integer
     */
    public function getCourierId()
    {
        return $this->courier_id;
    }

    /**
     * Set city
     *
     * @param City $city
     *
     * @return Schedule
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set courier
     *
     * @param Courier $courier
     *
     * @return Schedule
     */
    public function setCourier(Courier $courier = null)
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * Get courier
     *
     * @return Courier
     */
    public function getCourier()
    {
        return $this->courier;
    }
}