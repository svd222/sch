<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 20.08.18
 * Time: 23:28
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use System\Component;

/**
 * @Entity @Table(name="cities")
 **/
class City extends Component
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="integer") **/
    protected $distance_to_moscow_in_hours;

    /** @Column(type="string") **/
    public $name;

    /**
     * @OneToMany(targetEntity="App\Entity\Schedule", mappedBy="city", cascade={"remove"}, fetch="EAGER")
     */
    protected $schedules;

    public function __construct()
    {
        $this->schedules = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set distanceToMoscowInHours
     *
     * @param integer $distanceToMoscowInHours
     *
     * @return City
     */
    public function setDistanceToMoscowInHours($distanceToMoscowInHours)
    {
        $this->distance_to_moscow_in_hours = $distanceToMoscowInHours;

        return $this;
    }

    /**
     * Get distanceToMoscowInHours
     *
     * @return integer
     */
    public function getDistanceToMoscowInHours()
    {
        return $this->distance_to_moscow_in_hours;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}