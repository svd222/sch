<?php
use Symfony\Component\Templating\Helper\SlotsHelper;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;

$assetPath = new PathPackage(
    '/web',
    new StaticVersionStrategy('v1')
);

/**
 * @var SlotsHelper $view
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bootswatch: United</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="<?= $assetPath->getUrl('css/custom.min.css'); ?>">
    <link rel="stylesheet" href="<?= $assetPath->getUrl('css/united.css'); ?>">
    <link rel="stylesheet" href="<?= $assetPath->getUrl('css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= $assetPath->getUrl('css/jquery.datetimepicker.min.css'); ?>">
    <link rel="stylesheet" href="<?= $assetPath->getUrl('css/jquery.gritter.css'); ?>">
    <link rel="stylesheet" href="<?= $assetPath->getUrl('css/custom.css'); ?>">
    <?php $view['slots']->output('styles') ?>
</head>
<body>
<div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
    <div class="container">
        <a href="/" class="navbar-brand">Test app</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/schedule/list">Schedule</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/schedule/generator">Schedule generator</a>
                </li>
            </ul>

            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/schedule/city">Check cities list</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/schedule/courier">Check courier list</a>
                </li>
            </ul>

        </div>
    </div>
</div>


<div class="container">

    <?php $view['slots']->output('_content') ?>

</div>

<script src="<?php echo $assetPath->getUrl('js/jquery.min.js'); ?>"></script>
<script src="<?php echo $assetPath->getUrl('js/popper.min.js'); ?>"></script>
<script src="<?php echo $assetPath->getUrl('js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo $assetPath->getUrl('js/jquery.gritter.js'); ?>"></script>
<script src="<?php echo $assetPath->getUrl('js/jquery.datetimepicker.full.js'); ?>"></script>
<script src="<?php echo $assetPath->getUrl('js/custom.js'); ?>"></script>
<script src="<?php echo $assetPath->getUrl('js/gritter-notice.js'); ?>"></script>
<?php $view['slots']->output('scripts') ?>
</body>
</html>
