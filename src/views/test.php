<?php
use Symfony\Component\Templating\PhpEngine;
/**
 *
 * @var PhpEngine $view
 */
?>
<?php $view->extend('layout.php') ?>

<div class="container clearfix">
    <div class="row">
        <div class="col-md-3">
            some content
        </div>
        <div class="col-md-9">
            more content
        </div>
    </div>
</div>

