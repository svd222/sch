<?php

use Symfony\Component\Templating\PhpEngine;
use App\Entity\City;

/**
 * @var PhpEngine $view
 * @var City[] $cities
 */
?>
<?php $view->extend('layout.php') ?>

<div class="container clearfix">
    <div class="row">
        <div class="offset-2"></div>
        <div class="col-7">
            <h1>City list</h1>
        </div>
    </div>
    <div class="row">
        <div class="offset-2"></div>
        <div class="col-7">
            <form action="/schedule/add" method="post">
                <table class="table">
                    <tr>
                        <td>
                            City
                        </td>
                        <td>
                            <select name="city" id="city">
                                <option value="0">Select city...</option>
                                <?php
                                    foreach ($cities as $city):
                                ?>
                                <option value="<?= $city->getId(); ?>"><?= $city->getName(); ?></option>
                                <?php
                                    endforeach;
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Courier
                        </td>
                        <td>
                            <select name="courier" id="courier">
                                <option value="0">Select courier...</option>
                                <option value="1">Select 1</option>
                                <option value="2">Select 2</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Departure date
                        </td>
                        <td>
                            <input type="text" name="departureDate" id="departureDate" class="dt-picker">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Arrival date
                        </td>
                        <td>
                            <span id="arrivalDate"></span>
                            <input id="arrival_date_input" name="arrival_date_input" type="hidden" />
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <input type="submit" value="Send" name="submit">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<?php
    $cityHash = array_map(function ($city) {
        /**
         * @var City $city
         */
        return [$city->getId(), $city->getDistanceToMoscowInHours()];
    }, $cities);

    $scripts = "
        <script>
            
            var cities = ".json_encode($cityHash).";
            $(document).ready(function(e) {
                var courier = $('#courier');
                var city = $('#city');
                var departureDate = $('#departureDate');
                
                var cityVal;
                var departureDateVal;
                
                /**
                * Reset select list 
                * @param obj
                */
                function reset(obj) {
                    if (obj.get(0).tagName == 'SELECT') {
                        obj.find('option').remove();
                        if (obj.prop('id') == 'courier') {
                            $('<option value=\"0\">Select courier...</option>').appendTo(obj);
                        }
                    } 
                }
                
                function fillAvailableCouriersList(dt) {
                    reset(courier);
                    $.ajax('/schedule/get-available-couriers-list', {
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            dt: dt.getFullYear() + '-' + dt.getMonth() + '-' + dt.getDate() + ' ' + dt.getHours() + ':' + dt.getMinutes(),
                        },
                        success: function (data, status, xhr) {
                            data.forEach(function(item) {
                                $('<option value=\"' + item[0] + '\">' + item[1] + '</option>').appendTo(courier);
                            });
                        },
                    });
                }
                
                function calculateArrivalDate() {
                    $.ajax('/schedule/get-city', {
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            city_id: city.val(),
                        },
                        success: function (distance, status, xhr) {
                            /*if (distance) {
                                departureDateVal = departureDate.val().trim();
                                if (!departureDateVal) {
                                    notice('Please select departure date');
                                    departureDate.focus();
                                } else {
                                    setArrivalDate();
                                }
                            }*/
                        },
                    });
                }
                
                function leadZero(val) {
                    if (val < 10) {
                        return '0' + val;
                    }
                    return val;
                }
            
                function dtChanged(cDt) {
                    fillAvailableCouriersList(cDt);   
                    var dt = cDt;
                    dt = dt.getFullYear() + '-' + leadZero(dt.getMonth() + 1) + '-' + leadZero(dt.getDate()) + ' ' + leadZero(dt.getHours()) + ':' + leadZero(dt.getMinutes());
                    console.log(dt);
                    cityId = city.val().trim();
                    if (!cityId) {
                        notice('Please select city');
                        city.focus();
                    } else {
                        var dMh = 0;
                        cities.forEach(function(cInfo) {
                            if (cityId == cInfo[0]) {
                                dMh = cInfo[1];
                            }
                        });
                        arrivalDate = cDt;
                        arrivalDate.setHours(cDt.getHours() + dMh);
                        $('#arrivalDate').text(arrivalDate);
                        //in YYYY-mm-dd HH:ii
                        var formattedDate = arrivalDate.getFullYear() + '-' + leadZero(arrivalDate.getMonth() + 1) + '-' + leadZero(arrivalDate.getDate()) + ' ';
                        formattedDate += leadZero(arrivalDate.getHours()) + ':' + leadZero(arrivalDate.getMinutes());
                        $('#arrival_date_input').val(formattedDate);
                    }
                }
            
                jQuery.datetimepicker.setLocale('ru');
                $('.dt-picker').datetimepicker({
                    format:'d.m.Y H:i',
                    inline:false,
                    lang:'ru',
                    theme:'dark',
                    onChangeDateTime:dtChanged,
                    //onShow:dtChanged
                });
                
                city.on('change', function(item) {
                    calculateArrivalDate();
                    
                });
            });
            
            var d = new Date();
            console.log(d);
            d.setHours(d.getHours() + 4);
            console.log(d);
        </script>";
    $view['slots']->set('scripts', $scripts);
?>

