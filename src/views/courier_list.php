<?php

use Symfony\Component\Templating\PhpEngine;
use App\Entity\Courier;

/**
 * @var PhpEngine $view
 * @var Courier[] $couriers
 */
?>
<?php $view->extend('layout.php') ?>

<div class="container clearfix">
    <div class="row">
        <div class="offset-3"></div>
        <div class="col-5">
            <h1>Courier list</h1>
        </div>
    </div>
    <div class="row">
        <div class="offset-3"></div>
        <div class="col-5">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Surname</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($couriers as $courier): ?>
                    <tr>
                        <th scope="row"><?= $courier->id; ?></th>
                        <td><?= $courier->surname; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
