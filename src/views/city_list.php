<?php

use Symfony\Component\Templating\PhpEngine;
use App\Entity\City;

/**
 * @var PhpEngine $view
 * @var City[] $cities
 */
?>
<?php $view->extend('layout.php') ?>

<div class="container clearfix">
    <div class="row">
        <div class="offset-2"></div>
        <div class="col-7">
            <h1>City list</h1>
        </div>
    </div>
    <div class="row">
        <div class="offset-2"></div>
        <div class="col-7">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Distance from Moscow (in hours)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cities as $city): ?>
                    <tr>
                        <th scope="row"><?= $city->id; ?></th>
                        <td><?= $city->name; ?></td>
                        <td><?= $city->getDistanceToMoscowInHours(); ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
