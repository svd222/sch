<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 21.08.18
 * Time: 22:32
 */
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use System\EntityManagerLoader;

require_once "vendor/autoload.php";

$confPath = realpath('./config/config.php');
$configuration = require_once $confPath;

// replace with mechanism to retrieve EntityManager in your app
$entityManager = (new EntityManagerLoader($configuration))->getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);